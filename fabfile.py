from fabric.api import task, local, lcd, env

@task
def profile(sort="calls"):
    local("python -m cProfile -s %s tadokutan.py" % sort)

@task
def test():
    local("./run_tests.py")

@task
def debug_run():
    local("python ./tadokutan.py -d")

@task
def test_run():
    test()
    debug_run()

@task
def run(subcommand=""):
    subcommand = subcommand.strip()
    cmds = {
        'debug': debug_run,
        'test': test_run,
    } 
    if not subcommand:
        debug_run()
    elif subcommand not in cmds:
        print "%s is not a vaild subcommand for `run`:" % subcommand
        print "fab run:\n\t",
        print "\n\t".join(cmds.keys())
    else:
        cmds[subcommand]()

@task
def commit_push(message=""):
    test()
    local("git add .")
    local("git commit -m \"%s\"" % message)
    local("git push origin")