# -*- coding: utf-8 -*-

import json
import logging

import settings

logger = logging.getLogger(__name__)

class TadokuAPIInvalidSetting(Exception):
    pass

def create_api_json(entry):
    """Creates JSON object for sending an update to the Tadoku app."""
    try:
        username   = settings.API_SETTINGS['username']
        auth_token = settings.API_SETTINGS['auth_token']
        if not username.strip() or not auth_token.strip():
            raise KeyError    # empty values are invalid
    except KeyError:
        logger.error("Cannot create API update JSON -- check "
                     "username and auth_token settings.")
        raise TadokuAPIInvalidSetting()

    api_update = {
        "username": username,
        "auth_token": auth_token,
        "update": {
            "value": entry.value,
            "type": entry.type,
            "times": entry.times,
        }
    }

    return json.dumps(api_update, ensure_ascii=False)
