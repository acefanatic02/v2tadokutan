# -*- coding: utf-8 -*-

import re
import ast
import sys
import datetime
import logging
import copy

from PyQt4.QtGui import *

from tadokutan import settings, log
from tadokutan.ui.ruleset_dlg import RulesetDialog
from tadokutan.ui.entries_dlg import EntriesDialog

logger = logging.getLogger(__name__)

# Regexes used in parsing entry values.
RE_EXPR = re.compile(ur"\((\d+\.?\d*\s?[+-]\s?\d+\.?\d*)\)")
RE_TIME = re.compile(ur"(\d{1,}):(\d{2}):(\d{2})")

class MainWindowController(object):
    """Controller class for the main Tadokutan window."""
    def __init__(self, window, log=None):
        self.window = window
        self.active_filename = settings.SETTINGS['last_opened_file']
        if log is not None:
            self.log = log
        else:
            self.load_log(self.active_filename)
        self.update_score()

    def update_score(self):
        self.window.ui.score.setText(str(self.log.get_score()))

    def new_log(self):
        """Called by File->New or the new file shortcut.  Creates a new, empty
        ReadingLog.  Prompts to save changes first."""
        if self.log.dirty:
            confirm = QMessageBox.question(self.window, "Save changes?",
                "Save changes before creating new log?\n"
                "Unsaved changes will be lost.\n",
                buttons=QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel,
                defaultButton=QMessageBox.Cancel)
            if confirm == QMessageBox.Cancel: 
                return
            elif confirm == QMessageBox.Yes:
                self.save()

        self.log = log.ReadingLog(settings.DEFAULT_RULESET)
        self.active_filename = ""
        self.update_score()

    def load_log(self, filename=None):
        if not filename:
            filename = QFileDialog.getOpenFileName(self.window, 
                caption="Open File",
                directory=settings.SETTINGS['last_opened_file'],
                filter="Tadoku Saves (*.tdku)")

        # If the file dialog is canceled, filename will be empty.
        if filename:
            filename = unicode(filename)  # convert from QString
            self.log = log.ReadingLog(settings.DEFAULT_RULESET)
            try:
                self.log.load(filename)
                self.active_filename = filename
                self.update_score()
            except IOError, ioerr:
                QMessageBox.warning(self.window, "File Open Error",
                    "Tadoku-tan encountered an error while opening the log.\n",
                    buttons=QMessageBox.Ok,
                    defaultButton=QMessageBox.Ok)

    def save(self):
        """Called by File->Save or the save shortcut.  Saves to active filename if
        it exists, otherwise calls Save As."""
        if self.active_filename:
            try:
                self.log.save(self.active_filename)
            except IOError, ioerr:
                QMessageBox.warning(self.window, "File Save Error",
                    "Tadoku-tan encountered an error while saving the log.\n",
                    buttons=QMessageBox.Ok,
                    defaultButton=QMessageBox.Ok)
        else:
            self.save_as()

    def save_as(self):
        """Called by File->Save As, the save-as shortcut, or by save() if there is
        no active filename.  Prompts user for filename and saves the log."""
        filename = QFileDialog.getSaveFileName(self.window,
            caption="Save File",
            directory=settings.SETTINGS.get('last_opened_file', ""),
            filter="Tadoku Saves (*.tdku)")

        # If the dialog is canceled, filename will be empty.
        if filename:
            filename = unicode(filename)
            try:
                self.log.save(filename)
                settings.SETTINGS['last_opened_file'] = filename
                self.active_filename = filename
            except IOError, ioerr:
                QMessageBox.warning(self.window, "File Save Error",
                    "Tadoku-tan encountered an error while saving the log.\n",
                    buttons=QMessageBox.Ok,
                    defaultButton=QMessageBox.Ok)

    def on_exit(self):
        """Called on exit attempt.  Prompts to save if changes exist, unless
        save_on_exit is set in the settings, in which case the log is saved
        without prompting.
        Returns True if the program may exit, False if the exit is aborted."""
        prompt = None
        if self.log.dirty and not settings.SETTINGS['save_on_exit']:
            # Unsaved changes -- do not prompt if autosave is on.
            prompt = QMessageBox.question(self.window, "Tadoku-tan",
                "There are unsaved changes to the current log.\n"
                "Would you like to save before exiting?\n",
                buttons=QMessageBox.Ok|QMessageBox.Cancel,
                defaultButton=QMessageBox.Ok)
        if prompt == QMessageBox.Ok or settings.SETTINGS['save_on_exit']:
            try:
                if self.active_filename:
                    self.log.save(self.active_filename)
                    settings.SETTINGS['last_opened_file'] = self.active_filename
                else:
                    self.save_as()
            except IOError, ioerr:
                confirm = QMessageBox.question(self.window, "File Save Error",
                    "Error saving file on exit.  Progress may be lost.\n"
                    "Are you sure you want to exit?\n",
                    buttons=QMessageBox.Ok|QMessageBox.Cancel,
                    defaultButton=QMessageBox.Cancel)
                if confirm == QMessageBox.Cancel:
                    # Abort exit
                    return False
            settings.save_settings()
            return True
        else:
            return False

    def open_ruleset_dialog(self):
        """Creates a ruleset dialog, using a copy of the log's ruleset.
        If the dialog is accepted by the user, updates the log's ruleset."""
        working = copy.deepcopy(self.log.ruleset)
        ruleset_dlg = RulesetDialog(working)
        if ruleset_dlg.exec_():
            self.log.update_ruleset(working)
            self.update_score()

    def open_entries_dialog(self):
        working = copy.deepcopy(self.log)
        entries_dlg = EntriesDialog(working)
        if entries_dlg.exec_():
            self.log = working
            self.update_score()

    def submit_clicked(self):
        """Called when submit button is clicked."""
        if not self.window.ui.media_bgroup.checkedButton():
            return  # Sanity check, cannot proceed with no type selected.
        entry_type = self.get_entry_type()
        
        try:
            # Values less than 1 do not make sense for `times`.
            times = max(int(self.window.ui.times_read.text()), 1)
        except ValueError:
            times = 1
            logger.debug("Invalid times read: \'%s\'" % 
                self.window.ui.times_read.text())

        try:
            value = self.parse_entry_value(str(self.window.ui.amount_read.text()), 
                        entry_type)
        except ValueError:
            if not isinstance(self.window, QMainWindow):
                return  # For unit testing purposes

            QMessageBox.information(self.window, "Invalid Entry", 
                "Tadoku-tan was unable to parse your entry!\n" +
                "Consult the documentation, double-check your entry, " +
                "and try again.",
                buttons=QMessageBox.Ok,
                defaultButton=QMessageBox.Ok)
            logger.info("Invalid entry submitted: %s" % 
                unicode(self.window.ui.amount_read.text()))
            return
        
        self.log.add_entry(value=max(value, 0.0), # Don't allow negative values
                           type=entry_type,
                           times=times,
                           comment=unicode(self.window.ui.entry_comment.text()))

        # Clear the entries only on success.
        self.window.ui.amount_read.clear()
        self.window.ui.times_read.clear()
        self.window.ui.entry_comment.clear()

        self.update_score()

        if settings.SETTINGS['save_on_entry_submit'] == True:
            self.save()

    def get_entry_type(self):
        """Returns selected entry type as string key."""
        entry_type = {
            self.window.ui.book_radio:     'book',
            self.window.ui.manga_radio:    'manga',
            self.window.ui.game_radio:     'game',
            self.window.ui.flgame_radio:   'fullgame',
            self.window.ui.net_radio:      'net',
            self.window.ui.news_radio:     'news',
            self.window.ui.nico_radio:     'nico',
            self.window.ui.subs_radio:     'subs',
            self.window.ui.lyric_radio:    'lyric',
            self.window.ui.sent_radio:     'sentences',
        }[self.window.ui.media_bgroup.checkedButton()]
        
        if (entry_type == 'book' and self.window.ui.book_dr_check.isChecked()):
            return 'book_dr'
        else:
            return entry_type

    @staticmethod
    def parse_entry_value(string, entry_type):
        """Parses input value to floating point.  Allows expressions of the
        form `([float][+-][float])` or `HH:MM:SS` (subs and nico only) as
        special cases, otherwise attempts to convert directly to float.
        Raises ValueError for invalid expressions."""
        expr_match = RE_EXPR.search(string)
        if expr_match is not None:
            expr_group = expr_match.group(0)
            if '-' in expr_group:
                operands = expr_group.strip("()").split('-')
                return float(ast.literal_eval(operands[0].strip()) - 
                             ast.literal_eval(operands[1].strip()))
            else:
                operands = expr_group.strip("()").split('+')
                return float(ast.literal_eval(operands[0].strip()) +
                             ast.literal_eval(operands[1].strip()))

        # TODO: Should these be configurable?
        if entry_type in ['subs', 'nico']:
            time_match = RE_TIME.search(string)
            if time_match is not None:
                hms = map(float, time_match.groups("0"))
                return (hms[0]*60.0) + (hms[1]) + (hms[2]/60.0)

        return float(string)
