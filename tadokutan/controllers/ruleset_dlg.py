# -*- coding: utf-8 -*-

import datetime
import logging

from PyQt4 import QtGui

from tadokutan import settings

logger = logging.getLogger(__name__)

class RulesetDialogController(object):
    """Controller object for the ruleset settings dialog."""
    def __init__(self, window, ruleset):
        super(RulesetDialogController, self).__init__()
        self.window = window
        self.ruleset = ruleset

    def accept(self):
        """Called when the user accepts the dialog."""
        new_ruleset = {
            'book':      self.window.ui.book_val.text(),
            'book_dr':   self.window.ui.book_dr_val.text(),
            'manga':     self.window.ui.manga_val.text(),
            'game':      self.window.ui.game_val.text(),
            'fullgame':  self.window.ui.fgame_val.text(),
            'net':       self.window.ui.net_val.text(),
            'news':      self.window.ui.news_val.text(),
            'nico':      self.window.ui.nico_val.text(),
            'subs':      self.window.ui.subs_val.text(),
            'lyric':     self.window.ui.lyric_val.text(),
            'sentences': self.window.ui.sent_val.text(),
        }
        for k in new_ruleset:
            try:
                self.ruleset[k] = float(new_ruleset[k])
            except ValueError:
                if not isinstance(self.window, QtGui.QDialog):
                    continue  # For unit testing

                logger.error("Invalid value for ruleset `%s`: %s" %
                             (k, new_ruleset[k]))

                QtGui.QMessageBox.warning(self.window, "Invalid value.",
                    "Invalid value for ruleset: %s." % (new_ruleset[k]),
                    buttons=QMessageBox.Ok)

    def load_ruleset(self):
        """Load a ruleset from a file and update the value fields."""
        ruleset_fn = QtGui.QFileDialog.getOpenFileName(self.window,
            caption="Load Ruleset",
            directory=settings.SETTINGS['ruleset_dir'],
            filter="JSON Files (*.json)")

        if ruleset_fn:
            try:
                self.ruleset = settings.load_ruleset(ruleset_fn)
            except IOError:
                QtGui.QMessageBox.warning(self.window, "Load Failed",
                    "An error occurred while loading the ruleset file.\n" +
                    "Check tadokutan.log for details.")
                return
            except ValueError:
                QtGui.QMessageBox.warning(self.window, "Load Failed",
                    "An error occured while decoding the ruleset JSON.\n" +
                    "Check tadokutan.log for details.")
                return
            self.window.update_fields()

    def save_ruleset(self):
        """Save the current ruleset to a new file."""
        filename = QtGui.QFileDialog.getSaveFileName(self.window,
            caption="Save Ruleset",
            directory=settings.SETTINGS['ruleset_dir'],
            filter="JSON Files (*.json)")

        if filename:
            try:
                settings.save_ruleset(self.ruleset, filename)
            except IOError:
                QtGui.QMessageBox.warning(self.window, "Save Failed",
                    "An error occurred while saving the ruleset file.\n" +
                    "Check tadokutan.log for details")
            except ValueError:
                QtGui.QMessageBox.warning(self.window, "Save Failed",
                    "An error occurred while encoding the ruleset JSON.\n" +
                    "Check tadokutan.log for details")

