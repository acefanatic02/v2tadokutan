# -*- coding: utf-8 -*-

import os
import sys
import json
import codecs
import logging

logger = logging.getLogger(__name__)

SYS_CODEC = sys.stderr.encoding or sys.stdout.encoding

SETTINGS = {}
API_SETTINGS = {}
DEFAULT_RULESET = {}

def load_ruleset(filename):
    """Load a ruleset from JSON and return as a Python dictionary.
    Re-raises exceptions."""
    try:
        with codecs.open(filename, encoding='utf-8') as f:
            return json.load(f)
    except IOError, ioerr:
        logger.error("Failed to load ruleset: %s" % 
            unicode(str(ioerr), SYS_CODEC))
        raise ioerr
    except ValueError, verr:
        logger.error("Failed to decode ruleset: %s" % unicode(verr))
        raise verr

def save_ruleset(ruleset, filename):
    """Save a ruleset to JSON.
    Re-raises exceptions."""
    try:
        with codecs.open(filename, 'w', encoding='utf-8') as f:
            json_str = json.dumps(ruleset, sort_keys=True,
                indent=4, separators=(',', ': '))
            f.write(json_str)
    except IOError, ioerr:
        logger.error("Failed to save ruleset: %s" % 
            unicode(str(ioerr), SYS_CODEC))
        raise ioerr
    except ValueError, verr:
        logger.error("Failed to encode ruleset: %s" % unicode(verr))
        raise verr

def get_settings(filename):
    """Load settings from JSON.
    Logs exceptions but does not re-raise -- returns None on error."""
    try:
        with codecs.open(filename, encoding='utf-8') as f:
            return json.load(f)
    except IOError, ioerr:
        logger.error(u"Error loading settings: %s" % 
            unicode(str(ioerr), SYS_CODEC))
    except ValueError, verr:
        logger.error(u"Error decoding settings JSON: %s" % (unicode(verr)))
    return None

def init_settings():
    """Initialize global settings dicts."""
    global SETTINGS, API_SETTINGS, DEFAULT_RULESET

    settings_dir = os.path.abspath("./settings")
    settings_fn  = os.path.join(settings_dir, "settings.json")
    SETTINGS = get_settings(settings_fn)

    api_settings_fn = os.path.join(settings_dir, "api.json")
    API_SETTINGS = get_settings(api_settings_fn)

    DEFAULT_RULESET = get_settings(os.path.join(settings_dir, 'rulesets', 'ja.json'))

def save_settings():
    """Saves the global settings dicts.  Called by settings dialog and
    on application exit.  Logs exceptions but does not re-raise them."""
    global SETTINGS, API_SETTINGS
    settings_dir = os.path.abspath("./settings")

    try:
        with codecs.open(os.path.join(settings_dir, "settings.json"),
            'w', encoding='utf-8') as f:
            f.write(json.dumps(SETTINGS, sort_keys=True,
                indent=4, separators=(',', ': ')))

        with codecs.open(os.path.join(settings_dir, "api.json"),
            'w', encoding='utf-8') as f:
            f.write(json.dumps(API_SETTINGS, sort_keys=True,
                indent=4, separators=(',', ': ')))
    except IOError, ioerr:
        logger.error("Failed to save settings: %s" % 
            unicode(str(ioerr), SYS_CODEC))
    except ValueError, verr:
        logger.error("Failed to encode settings: %s" % unicode(verr))
