# -*- coding: utf-8 -*-

import sys
import copy
import logging
import datetime

logger = logging.getLogger(__name__)

try:
    import cPickle as pickle
    logger.info(u"cPickle successfully imported.")
except ImportError:
    import pickle
    logger.info(u"Unable to import cPickle, using pickle instead.")

# Best guess for the encoding used for system error messages:
SYS_CODEC = sys.stderr.encoding or sys.stdout.encoding

from collections import namedtuple

TadokuEntry = namedtuple('TadokuEntry', 
                         ('value',
                          'type',
                          'times', 
                          'created_at', 
                          'comment'))

def entry_to_unicode(entry):
    tstmp_str = unicode(entry.created_at.strftime(u"%c"), "utf-8")
    return (u"Type:\t%s\n"
            u"Value:\t%.2f\n"
            u"Times:\t%s\n"
            u"Created:\t%s\n"
            u"Comment:\t%s\n"
            % (entry.type,
               entry.value,
               entry.times,
               tstmp_str,
               entry.comment))
TadokuEntry.__unicode__ = entry_to_unicode

class ReadingLog(object):
    """Class to hold and report on all Tadoku entries."""
    def __init__(self, ruleset):
        super(ReadingLog, self).__init__()
        self.entries = []
        self.ruleset = ruleset
        self.dirty = False

    def save(self, filename):
        """Pickle the entries list and save to file."""
        try:
            with open(filename, 'wb') as f:
                save_tuple = (self.ruleset, self.entries)
                pickle.dump(save_tuple, f)
                self.dirty = False
        except IOError, ioerr:
            logger.error("Error, ReadingLog save failed: %s", 
                unicode(str(ioerr), SYS_CODEC))
            raise ioerr

    def load(self, filename):
        """Load and un-Pickle the entries list."""
        try:
            with open(filename, 'rb') as f:
                (self.ruleset, self.entries) = pickle.load(f)
                self.dirty = False
        except IOError, ioerr:
            logger.error("Error, ReadingLog load failed: %s", 
                unicode(str(ioerr), SYS_CODEC))
            raise ioerr

    def add_entry(self, **kwargs):
        """Adds an entry to the log.

        Required arguments:
          value: integer, number of pages read (unadjusted)
          type: string, type of media read (Unknown types raise TypeError)
          times: integer, number of times read
        Optional arguments:
          comment: unicode string comment
        """
        for key in ['value', 'type', 'times']:
            if key not in kwargs:
                raise TypeError(u"Required argument not supplied for entry: %s"
                                % key)

        if kwargs['type'] not in self.ruleset:
            raise TypeError(u"Unknown type given to ReadingLog.add_entry(): %s" 
                            % kwargs['type'])

        entry = TadokuEntry(kwargs['value'],
                            kwargs['type'],
                            kwargs['times'],
                            datetime.datetime.now(),
                            kwargs['comment'] if 'comment' in kwargs else u"",)

        logger.info(u"Entry created at %s: %d|%s|%d times|%s" % 
                    (str(entry.created_at),
                     entry.value, 
                     entry.type, 
                     entry.times, 
                     entry.comment))

        self.entries.append(entry)
        self.dirty = True

    def delete_entry(self, timestamp):
        """Deletes an entry from the log keyed by timestamp -- returns the 
        deleted entry."""
        i = self._find_entry_by_tstmp(timestamp)
        entry = self.entries[i]
        del self.entries[i]
        self.dirty = True
        logger.info(u"Entry deleted: %s|%d|%s|%d times|%s" %
                    (str(entry.created_at),
                     entry.value,
                     entry.type,
                     entry.times,
                     entry.comment))
        return entry

    def update_ruleset(self, ruleset):
        """Replaces the instance ruleset with a copy of the given ruleset."""
        self.ruleset = copy.deepcopy(ruleset)
        self.dirty = True

    def _score(self, entry):
        """Calculate a single entry's score."""
        return ((entry.value * self.ruleset[entry.type]) * 
                (1.0 / (2 ** (entry.times - 1))))

    def get_score(self, typelist=None):
        """If typelist is None, returns total score of all entries.  Otherwise,
        returns total of all entries of the given types.  Unknown types raise
        TypeError."""
        total = 0
        if typelist == None:
            total = sum([self._score(entry) for entry in self.entries])
        else:
            for item in typelist:
                if item not in self.ruleset:
                    raise TypeError(
                        u"Unknown type given to ReadingLog.get_score: %s" % item)
            total = sum([self._score(entry) 
                         for entry 
                         in self.entries 
                         if entry.type in typelist])
        return total

    def get_value(self, type):
        """Returns cumulative value (unadjusted score) of a given entry type.
        Unknown types raise TypeError."""
        if type not in self.ruleset:
            raise TypeError(u"Unknown type given to ReadingLog.get_value")
        return sum([entry.value 
                    for entry 
                    in self.entries 
                    if entry.type == type])

    def get_entries_for_range(self, start_time, stop_time):
        """Returns a list of entries with timestamps in the range 
        (start_time, stop_time]."""
        return [entry
                for entry
                in self.entries
                if start_time < entry.created_at <= stop_time]

    def score_list(self, entry_list):
        """Scores a list (or other iterable) of entries according to the 
        current ruleset."""
        return sum([self._score(entry) for entry in entry_list])

    def get_entry_by_tstmp(self, timestamp):
        """Finds and returns entry based on given timestamp."""
        index = self._find_entry_by_tstmp(timestamp)
        return self.entries[index]

    def _find_entry_by_tstmp(self, timestamp):
        """Searches entry list for an entry with the given timestamp.
        Returns the index of found entry, or None if not found."""
        low = 0
        high = len(self.entries)
        while high >= low:
            mid = low + ((high - low) / 2)
            entry = self.entries[mid]
            if entry.created_at == timestamp:
                return mid  # Found!
            elif entry.created_at > timestamp:
                high = (mid - 1)
            else:
                low = (mid + 1)
        return None  # Not found.
