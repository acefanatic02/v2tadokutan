# -*- coding: utf-8 -*-

# Stat plotting:
# create list of tuples
# [(datetime, value),
#  (datetime, value)]

def get_times_with_delta(start, end, delta):
    """Returns a list of datetimes between start and end, with given delta."""
    if not delta:
        raise ValueError("Invalid delta, must be non-zero.")
    elif ((start < end and delta.total_seconds() < 0) or
          (start > end and delta.total_seconds() > 0)):
        raise ValueError("Invalid range: start %s, end: %s, delta: %s"
            % (str(start), str(end), str(delta)))
    times = []
    cur = start
    if delta.total_seconds() > 0:
        while cur <= end:
            times.append(cur)
            cur += delta
    else:
        while cur >= end:
            times.append(cur)
            cur += delta
    return times

def plot_scores(log, start_dt, end_dt, delta, typelist=None):
    """Returns a list of tuples of (datetime, score) for the given date range
    and delta, filtered by typelist."""
    plot = []
    times = get_times_with_delta(start_dt, end_dt, delta)
    for t in times:
        if typelist:
            entries = [entry for entry 
                       in log.get_entries_for_range(start_dt, t) 
                       if entry.type in typelist]
        else:
            entries = log.get_entries_for_range(start_dt, t)
        v = log.score_list(entries)
        plot.append((t, v))
    return plot

def plot_datetime_to_float(plot):
    """Converts the datetime coordinate of a plot to a float."""
    first_dt = plot[0][0]
    return [((dt - first_dt).total_seconds(), v) for dt, v in plot]
    
