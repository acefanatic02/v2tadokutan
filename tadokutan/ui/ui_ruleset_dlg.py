# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class RulesetDialogUi(object):

    def __init__(self):
        super(RulesetDialogUi, self).__init__()

    def setup_ui(self, window):
        window.setFixedSize(350, 400)
        window.setWindowTitle(_fromUtf8("Ruleset Settings"))
        window.setModal(True)

        # Input
        self.book_val    = QtGui.QLineEdit()
        self.book_dr_val = QtGui.QLineEdit()
        self.manga_val   = QtGui.QLineEdit()
        self.game_val    = QtGui.QLineEdit()
        self.fgame_val   = QtGui.QLineEdit()
        self.net_val     = QtGui.QLineEdit()
        self.news_val    = QtGui.QLineEdit()
        self.nico_val    = QtGui.QLineEdit()
        self.subs_val    = QtGui.QLineEdit()
        self.lyric_val   = QtGui.QLineEdit()
        self.sent_val    = QtGui.QLineEdit()

        # Validation
        self.validator = QtGui.QDoubleValidator()
        for i in [self.book_val, self.book_dr_val, self.manga_val, self.game_val,
                  self.fgame_val, self.news_val, self.news_val, self.nico_val, 
                  self.subs_val, self.lyric_val, self.sent_val]:
            i.setValidator(self.validator)

        # Labels
        self.book_lb    = QtGui.QLabel(_fromUtf8("Book"))
        self.book_dr_lb = QtGui.QLabel(_fromUtf8("Book (DR)"))
        self.manga_lb   = QtGui.QLabel(_fromUtf8("Manga"))
        self.game_lb    = QtGui.QLabel(_fromUtf8("Game"))
        self.fgame_lb   = QtGui.QLabel(_fromUtf8("Full Game"))
        self.net_lb     = QtGui.QLabel(_fromUtf8("Net"))
        self.news_lb    = QtGui.QLabel(_fromUtf8("News"))
        self.nico_lb    = QtGui.QLabel(_fromUtf8("Nico"))
        self.subs_lb    = QtGui.QLabel(_fromUtf8("Subtitles"))
        self.lyric_lb   = QtGui.QLabel(_fromUtf8("Lyrics"))
        self.sent_lb    = QtGui.QLabel(_fromUtf8("Sentences"))

        # Buttons
        self.button_box = QtGui.QDialogButtonBox(window)
        self.button_box.setStandardButtons(QtGui.QDialogButtonBox.Ok|
                                           QtGui.QDialogButtonBox.Cancel)
        self.button_box.setOrientation(QtCore.Qt.Vertical)

        self.load_button = QtGui.QPushButton(_fromUtf8("Load..."))
        self.save_button = QtGui.QPushButton(_fromUtf8("Save..."))

        self.setup_layouts(window)

    def setup_layouts(self, window):
        self.layout = QtGui.QGridLayout(window)

        self.layout.addWidget(self.book_lb, 1, 1)
        self.layout.addWidget(self.book_dr_lb, 2, 1)
        self.layout.addWidget(self.manga_lb, 3, 1)
        self.layout.addWidget(self.game_lb, 4, 1)
        self.layout.addWidget(self.fgame_lb, 5, 1)
        self.layout.addWidget(self.net_lb, 6, 1)
        self.layout.addWidget(self.news_lb, 7, 1)
        self.layout.addWidget(self.nico_lb, 8, 1)
        self.layout.addWidget(self.subs_lb, 9, 1)
        self.layout.addWidget(self.lyric_lb, 10, 1)
        self.layout.addWidget(self.sent_lb, 11, 1)

        self.layout.addWidget(self.book_val, 1, 2)
        self.layout.addWidget(self.book_dr_val, 2, 2)
        self.layout.addWidget(self.manga_val, 3, 2)
        self.layout.addWidget(self.game_val, 4, 2)
        self.layout.addWidget(self.fgame_val, 5, 2)
        self.layout.addWidget(self.net_val, 6, 2)
        self.layout.addWidget(self.news_val, 7, 2)
        self.layout.addWidget(self.nico_val, 8, 2)
        self.layout.addWidget(self.subs_val, 9, 2)
        self.layout.addWidget(self.lyric_val, 10, 2)
        self.layout.addWidget(self.sent_val, 11, 2)

        self.layout.addWidget(self.button_box, 10, 3, 2, 1)
        self.layout.addWidget(self.load_button, 1, 3)
        self.layout.addWidget(self.save_button, 2, 3)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    RulesetDialog = QtGui.QDialog()
    ui = RulesetDialogUi()
    ui.setup_ui(RulesetDialog)
    RulesetDialog.show()
    sys.exit(app.exec_())