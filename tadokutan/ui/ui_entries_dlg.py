# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class EntriesDialogUi(object):

    def __init__(self):
        super(EntriesDialogUi, self).__init__()

    def setup_ui(self, window):
        window.resize(600, 250)
        window.setWindowTitle(_fromUtf8("Entries"))
        window.setModal(True)

        self.button_box = QtGui.QDialogButtonBox(window)
        self.button_box.setOrientation(QtCore.Qt.Vertical)
        self.button_box.setStandardButtons(QtGui.QDialogButtonBox.Cancel|
                                           QtGui.QDialogButtonBox.Ok)
        self.button_box.accepted.connect(window.accept)
        self.button_box.rejected.connect(window.reject)

        self.delete_button = QtGui.QPushButton(window)
        self.delete_button.setText(_fromUtf8("Delete Entry"))

        self.table = QtGui.QTableWidget(window)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.table.setRowCount(0)
        self.table.setColumnCount(4)

        self.table.horizontalHeader().setVisible(True)
        self.table.horizontalHeader().setDefaultSectionSize(120)
        self.table.horizontalHeader().setSortIndicatorShown(False)
        self.table.horizontalHeader().setStretchLastSection(True)

        self.table.verticalHeader().setVisible(False)

        self.table.setHorizontalHeaderLabels([
            "Created At", "Type", "Value", "Times Read"])

        self.setup_layout(window)

    def setup_layout(self, window):
        self.button_layout = QtGui.QVBoxLayout()
        self.button_layout.addWidget(self.button_box)
        self.button_layout.addWidget(self.delete_button)

        self.main_layout = QtGui.QHBoxLayout(window)
        self.main_layout.addWidget(self.table)
        self.main_layout.addItem(self.button_layout)


if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    window = QtGui.QDialog()
    ui = EntriesDialogUi()
    ui.setup_ui(window)
    window.show()
    sys.exit(app.exec_())