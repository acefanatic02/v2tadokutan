# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class MainWindowUi(object):

    def __init__(self):
        super(MainWindowUi, self).__init__()

    def setup_ui(self, window):
        window.setFixedSize(400, 300)
        window.setWindowTitle(_fromUtf8("Tadoku-tan"))
        self.centralwidget = QtGui.QWidget(window)
        window.setCentralWidget(self.centralwidget)

        self.setup_media_buttons(window)
        self.setup_menu_and_status_bars(window)
        self.setup_entry_and_submit(window)
        self.setup_layouts(window)

    def setup_media_buttons(self, window):
        # Setup group box surrounding the media selections
        self.media_box = QtGui.QGroupBox(self.centralwidget)
        self.media_box.setGeometry(QtCore.QRect(225, 0, 200, 250))
        self.media_box.setTitle("Media:")
        self.media_bgroup = QtGui.QButtonGroup(self.media_box)

        # Create media radio buttons and checkboxes
        self.book_radio     = QtGui.QRadioButton(_fromUtf8("Book"), parent=self.media_box)
        self.manga_radio    = QtGui.QRadioButton(_fromUtf8("Manga"), parent=self.media_box)
        self.flgame_radio   = QtGui.QRadioButton(_fromUtf8("Full Game"), parent=self.media_box)
        self.game_radio     = QtGui.QRadioButton(_fromUtf8("Game"), parent=self.media_box)
        self.net_radio      = QtGui.QRadioButton(_fromUtf8("Net"), parent=self.media_box)
        self.news_radio     = QtGui.QRadioButton(_fromUtf8("News"), parent=self.media_box)
        self.nico_radio     = QtGui.QRadioButton(_fromUtf8("Nico"), parent=self.media_box)
        self.subs_radio     = QtGui.QRadioButton(_fromUtf8("Subtitles"), parent=self.media_box)
        self.sent_radio     = QtGui.QRadioButton(_fromUtf8("Sentences"), parent=self.media_box)
        self.lyric_radio    = QtGui.QRadioButton(_fromUtf8("Lyrics"), parent=self.media_box)
        self.book_dr_check  = QtGui.QCheckBox(_fromUtf8("Double-rowed"), parent=self.media_box)

        # Link radio buttons to button group
        self.media_bgroup.addButton(self.book_radio)
        self.media_bgroup.addButton(self.manga_radio)
        self.media_bgroup.addButton(self.flgame_radio)
        self.media_bgroup.addButton(self.game_radio)
        self.media_bgroup.addButton(self.net_radio)
        self.media_bgroup.addButton(self.news_radio)
        self.media_bgroup.addButton(self.nico_radio)
        self.media_bgroup.addButton(self.subs_radio)
        self.media_bgroup.addButton(self.sent_radio)
        self.media_bgroup.addButton(self.lyric_radio)

        # Some misc setup
        self.media_bgroup.setExclusive(True)
        self.book_radio.setChecked(True)
        self.book_radio.toggled.connect(self._dr_tie)

    def setup_menu_and_status_bars(self, window):
        # Status bar:
        self.statusbar = QtGui.QStatusBar(window)
        window.setStatusBar(self.statusbar)
        self.score = QtGui.QLabel(_fromUtf8("0"))
        self.statusbar.addWidget(QtGui.QLabel(_fromUtf8(" Current Score: ")))
        self.statusbar.addWidget(self.score)

        # Menu bar:
        self.menubar = QtGui.QMenuBar(window)
        window.setMenuBar(self.menubar)

        # File menu
        self.menu_file = QtGui.QMenu(self.menubar)
        self.menu_file.setTitle(_fromUtf8("&File"))
        self.action_new  = QtGui.QAction(window)
        self.action_load = QtGui.QAction(window)
        self.action_save = QtGui.QAction(window)
        self.action_save_as = QtGui.QAction(window)
        self.action_exit = QtGui.QAction(window)

        self.action_new.setText(_fromUtf8("&New..."))
        self.action_new.setShortcut(QtGui.QKeySequence.New)

        self.action_load.setText(_fromUtf8("&Open..."))
        self.action_load.setShortcut(QtGui.QKeySequence.Open)
        
        self.action_save.setText(_fromUtf8("&Save"))
        self.action_save.setShortcut(QtGui.QKeySequence.Save)
        
        self.action_save_as.setText(_fromUtf8("Save &As..."))
        self.action_save_as.setShortcut(QtGui.QKeySequence.SaveAs)
        
        self.action_exit.setText(_fromUtf8("E&xit"))
        self.action_exit.setShortcut(QtGui.QKeySequence.Quit)

        self.menu_file.addAction(self.action_new)
        self.menu_file.addAction(self.action_load)
        self.menu_file.addAction(self.action_save)
        self.menu_file.addAction(self.action_save_as)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_exit)

        # View menu
        self.menu_view = QtGui.QMenu(self.menubar)
        self.menu_view.setTitle(_fromUtf8("&View"))
        self.action_stats = QtGui.QAction(window)
        self.action_stats.setText(_fromUtf8("S&tats..."))
        self.action_entries = QtGui.QAction(window)
        self.action_entries.setText(_fromUtf8("&Entries..."))
        self.menu_view.addAction(self.action_stats)
        self.menu_view.addAction(self.action_entries)

        # Preferences menu
        self.menu_pref = QtGui.QMenu(self.menubar)
        self.menu_pref.setTitle(_fromUtf8("&Preferences"))
        self.action_ruleset  = QtGui.QAction(window)
        self.action_settings = QtGui.QAction(window)
        self.action_ruleset.setText(_fromUtf8("&Ruleset Settings..."))
        self.action_settings.setText(_fromUtf8("&General Settings..."))

        self.menu_pref.addAction(self.action_ruleset)
        self.menu_pref.addAction(self.action_settings)

        self.menubar.addMenu(self.menu_file)
        self.menubar.addMenu(self.menu_view)
        self.menubar.addMenu(self.menu_pref)

    def setup_entry_and_submit(self, window):
        # Input
        self.amount_read   = QtGui.QLineEdit(self.centralwidget)
        self.times_read    = QtGui.QLineEdit(self.centralwidget)
        self.times_read.setMaximumWidth(30)
        self.entry_comment = QtGui.QLineEdit(self.centralwidget)

        # Buttons
        self.submit_button = QtGui.QPushButton(self.centralwidget)
        self.submit_button.setText(_fromUtf8("Submit"))

    def setup_layouts(self, window):
        self.main_layout = QtGui.QHBoxLayout(self.centralwidget)
        self.submit_layout = QtGui.QGridLayout(self.centralwidget)
        self.main_layout.addLayout(self.submit_layout)
        self.main_layout.addWidget(self.media_box)

        # Setup sub-layout for the media buttons
        self.media_grid = QtGui.QVBoxLayout()
        self.media_grid.setGeometry(QtCore.QRect(225, 0, 200, 250))
        self.media_grid.addWidget(self.book_radio)
        self.media_grid.addWidget(self.manga_radio)
        self.media_grid.addWidget(self.flgame_radio)
        self.media_grid.addWidget(self.game_radio)
        self.media_grid.addWidget(self.news_radio)
        self.media_grid.addWidget(self.nico_radio)
        self.media_grid.addWidget(self.subs_radio)
        self.media_grid.addWidget(self.sent_radio)
        self.media_grid.addWidget(self.lyric_radio)
        self.media_grid.addWidget(self.net_radio)
        self.media_grid.addWidget(self.book_dr_check)
        self.media_grid.addStretch(1)
        self.media_box.setLayout(self.media_grid)

        # Submit Layout
        self.submit_layout.setVerticalSpacing(10)
        self.submit_layout.addWidget(QtGui.QLabel(_fromUtf8("Amount Read:")),
                                     0, 1, 1, 6)
        self.submit_layout.addWidget(self.amount_read, 1, 1, 1, 2)
        self.submit_layout.addWidget(QtGui.QLabel(_fromUtf8("Times:")),
                                   1, 4, 1, 1)
        self.submit_layout.addWidget(self.times_read, 1, 5, 1, 1)
        self.submit_layout.addWidget(self.entry_comment, 2, 1, 1, 5)
        self.submit_layout.addWidget(self.submit_button, 10, 1, 1, 2)
        self.submit_layout.setRowStretch(3, 10)

    def _dr_tie(self):
        self.book_dr_check.setEnabled(self.book_radio.isChecked())


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = MainWindowUi()
    ui.setup_ui(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
