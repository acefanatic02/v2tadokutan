# -*- coding: utf-8 -*-

from PyQt4.QtGui import QDialog

from tadokutan.controllers.ruleset_dlg import RulesetDialogController
from tadokutan.ui.ui_ruleset_dlg import RulesetDialogUi

class RulesetDialog(QDialog):
    """Window management class for the ruleset settings dialog."""
    def __init__(self, ruleset):
        super(RulesetDialog, self).__init__()
        self.controller = RulesetDialogController(self, ruleset)
        self.ui = RulesetDialogUi()
        self.ui.setup_ui(self)
        self.update_fields()

        self.ui.load_button.clicked.connect(self.controller.load_ruleset)
        self.ui.save_button.clicked.connect(self.controller.save_ruleset)

        self.ui.button_box.accepted.connect(self.accept)
        self.ui.button_box.rejected.connect(self.reject)

    def accept(self):
        self.controller.accept()
        QDialog.accept(self)

    def update_fields(self):
        """Updates text for each field based on the instance ruleset."""
        for key, field in [
                ('book',      self.ui.book_val),
                ('book_dr',   self.ui.book_dr_val),
                ('manga',     self.ui.manga_val),
                ('game',      self.ui.game_val),
                ('fullgame',  self.ui.fgame_val),
                ('net',       self.ui.net_val),
                ('news',      self.ui.news_val),
                ('nico',      self.ui.nico_val),
                ('subs',      self.ui.subs_val),
                ('lyric',     self.ui.lyric_val),
                ('sentences', self.ui.sent_val)]:
            field.setText(str(self.controller.ruleset[key]))
