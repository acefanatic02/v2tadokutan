# -*- coding: utf-8 -*-

from PyQt4.QtGui import QMainWindow

from tadokutan.controllers.mainwindow import MainWindowController
from tadokutan.ui.ui_mainwindow import MainWindowUi

class MainWindow(QMainWindow):
    """Window class for the main window.  Owns a reference to UI instance
    and controller instance."""
    def __init__(self, log=None):
        super(MainWindow, self).__init__()
        self.ui = MainWindowUi()
        self.ui.setup_ui(self)
        self.controller = MainWindowController(self, log)

        # Setup triggers:
        self.ui.action_new.triggered.connect(self.controller.new_log)
        self.ui.action_load.triggered.connect(self.controller.load_log)
        self.ui.action_save.triggered.connect(self.controller.save)
        self.ui.action_save_as.triggered.connect(self.controller.save_as)
        self.ui.action_exit.triggered.connect(self.close)

        self.ui.action_entries.triggered.connect(self.controller.open_entries_dialog)

        self.ui.action_ruleset.triggered.connect(self.controller.open_ruleset_dialog)
        
        self.ui.submit_button.clicked.connect(self.controller.submit_clicked)
        self.ui.amount_read.returnPressed.connect(self.controller.submit_clicked)
        self.ui.times_read.returnPressed.connect(self.controller.submit_clicked)
        self.ui.entry_comment.returnPressed.connect(self.controller.submit_clicked)

    def closeEvent(self, event):
        """Override for the close event handler."""
        if self.controller.on_exit():
            event.accept()
        else:
            event.ignore()