# -*- coding: utf-8 -*-

import datetime

from PyQt4 import QtGui

from tadokutan.ui.ui_entries_dlg import EntriesDialogUi

class EntriesDialog(QtGui.QDialog):

    def __init__(self, log):
        super(EntriesDialog, self).__init__()
        self.log = log
        self.ui = EntriesDialogUi()
        self.ui.setup_ui(self)
        self.update_table()
        
        self.ui.delete_button.clicked.connect(self.delete_entry)
        self.ui.table.cellDoubleClicked.connect(self.entry_details)

    def get_entry_for_row(self, row):
        t_str = str(self.ui.table.item(row, 0).text())
        timestamp = datetime.datetime.strptime(t_str, 
            "%Y-%m-%d %H:%M:%S.%f")
        return self.log.get_entry_by_tstmp(timestamp)

    def entry_details(self, row, col):
        """Called when a cell is double-clicked.
        Displays a popup dialog with the entry details."""
        entry = self.get_entry_for_row(row)
        popup = QtGui.QMessageBox(self)
        popup.setWindowTitle("Entry Details")
        popup.setText(unicode(entry))
        popup.setIcon(QtGui.QMessageBox.NoIcon)
        popup.setStandardButtons(QtGui.QMessageBox.Close)
        popup.setModal(False)
        popup.show()

    def update_table(self):
        """Updates UI table with entry data."""
        if not self.log.entries:
            self.ui.table.clearContents()
            self.ui.table.setRowCount(0)
            # Can't delete from empty table
            self.ui.delete_button.setEnabled(False)
        else:
            # Turn off sorting to preserve rows
            self.ui.table.setSortingEnabled(False)
            self.ui.table.setRowCount(len(self.log.entries))

            for (row, entry) in enumerate(self.log.entries):
                for (col, item) in enumerate([
                        entry.created_at,
                        entry.type,
                        entry.value,
                        entry.times]):
                    tbl_item = QtGui.QTableWidgetItem(str(item))
                    self.ui.table.setItem(row, col, tbl_item)

            # Reenable sorting, ensure delete button is active.
            self.ui.table.setSortingEnabled(True)
            self.ui.delete_button.setEnabled(True)

    def delete_entry(self):
        """Called when the delete button is pressed.
        Asks user to confirm, then deletes the selected entry."""
        row = self.ui.table.currentRow()

        entry = self.get_entry_for_row(row)

        confirm = QtGui.QMessageBox.warning(self, "Delete Entry?",
            "Are you sure you want to delete this entry?\n" +
            "Value: %.2f, Type: \'%s\'" % (entry.value, entry.type),
            buttons=QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
            defaultButton=QtGui.QMessageBox.Cancel)

        if confirm == QtGui.QMessageBox.Ok:
            self.log.delete_entry(entry.created_at)
            self.update_table()