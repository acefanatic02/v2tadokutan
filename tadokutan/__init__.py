#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = '0.1.0'
__author__ = 'ACEfanatic02'
__contact__ = {
    'email': "acefanatic02@gmail.com",
    'irc': 'irc.rizon.net #ajatt'
}

## ## ## ## ##

import sys
import os.path
import logging
import argparse

from PyQt4 import QtCore, QtGui

from tadokutan import settings
from tadokutan import log
from tadokutan.ui.mainwindow import MainWindow

def main():
    parser = argparse.ArgumentParser(description="Tadoku-tan -- "
                                    "Offline Reading Contest Log")
    parser.add_argument('-v', '--version', 
                        action="version", 
                        version=__version__)
    parser.add_argument('-d', '--debug', 
                        action="store_true",
                        help="log debug messages")
    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.ERROR
    log_file = os.path.abspath("tadokutan.log")
    logging.basicConfig(level=log_level,
        filename=log_file,
        format='%(asctime)s %(levelname)s %(module)s:%(lineno)d : %(message)s')

    settings.init_settings()

    readinglog = None
    if not settings.SETTINGS['last_opened_file']:
        ruleset = settings.DEFAULT_RULESET
        readinglog = log.ReadingLog(ruleset)

    app = QtGui.QApplication(sys.argv)
    mainwindow = MainWindow(readinglog)
    mainwindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
