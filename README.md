# Tadoku-tan

Tadoku-tan is a desktop application to log reading in the same manner as the [Tadoku](http://readmod.com/) reading contest online.

## Why Tadoku-tan?
The Tadoku contest only runs for, at most, a month at a time, and scores do not roll over between rounds.  While this is understandable for a contest, I wanted to have a way to track reading over the long term.  Tadoku-tan is not intended to encourage quick sprints of reading, but instead focuses on encoraging a steady habit of reading over a period of months and years.

## Dependencies
Running Tadoku-tan requires:

* Python 2.7 (__Not__ Python 3)
* PyQt4

## User Guide
To run Tadoku-tan, open a terminal, cd to the root program directory (the first tadokutan/ directory) and run `$ python ./tadokutan.py`.  Depending on your OS setup, you may be able to simply double-click `tadokutan.py` in your file browser, or create a shortcut or symlink elsewhere.  At this point, there is no way to *install* Tadoku-tan.

To log an entry, enter the number of pages/screens/minutes/etc. read (or certain expressions, see below) in the text box labeled `Amount Read`, the number of times read in the smaller text box to the right, and select the correct media from the list to the right.  The text box below the `Amount Read` box is a comment field you can use for anything else you'd like to note about the entry.  (Such as recording which book you're reading.)

### Special Entry Expressions
Tadoku-tan supports a couple of special expressions for the `Amount Read` field.

Expressions of the form `(100-10)` or `(20+20)` are evaluated (90 and 40, respectively) before being logged as an entry.  So, if you've logged 10 pages of your book already, and are now on page 100, you can let Tadoku-tan take care of the arithmetic and save yourself some hassle.

Time-based entries (Subtitles and NicoNico) can be entered as `HH:MM:SS` (Hours:Minutes:Seconds).  For example, `01:20:30` will be logged as 80.5 minutes.  This saves you a bit of error-prone calculation.  (These entries will still interpret plain numbers as minute values if you prefer.)

### Settings
Ruleset settings can be found under the Preferences menu in the menu bar.  Rulesets are saved as part of the Tadoku Save file, and therefore you can log several languages by using multiple save files.

General settings are not currently editable from within Tadoku-tan.  Instead, they are found in the `settings/settings.json` file under the root program directory.

The available settings are as follows:

* "last_opened_file": used by Tadoku-tan for managing autosave.  Do not edit this manually.
* "ruleset_dir": indicates where ruleset settings files are saved.
* "save_on_entry_submit": true/false value.  If true, Tadoku-tan will automatically save the file after each entry is submitted.
* "save_on_exit": true/false value.  If true, Tadoku-tan will automatically save on program exit.

## Bugs and Feature Requests
To report bugs, please use the issue [tracker](https://bitbucket.org/acefanatic02/v2tadokutan/issues) on the Bitbucket repository.  Provide as much detail as you can about the problem, and attach `tadokutan.log` (found in the main Tadoku-tan directory.)

Feature requests are also welcome.  Open an issue on the [tracker](https://bitbucket.org/acefanatic02/v2tadokutan/issues) with the type "proposal" and it will be considered.  Please put forth some effort in your proposal; sloppily written requests will be closed unread.

## Contributing
Bugfixes, new features, and other contributions are welcome.  Please open an issue on the Bitbucket issue [tracker](https://bitbucket.org/acefanatic02/v2tadokutan/issues) to discuss your changes before submitting a pull request.  All contributions *must* be submitted with test coverage.  Follow the style conventions of local code, not Qt4's (except where necessary to interact with library code.)
