#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lib.test_runner import ModuleTestRunner

from tests.api_tests import TestAPI
from tests.log_tests import TestLog
from tests.tadoku_entry_tests import TestTadokuEntry
from tests.stats_tests import TestStats
from tests.mainwindow_tests import TestMainWindowController
from tests.ruleset_dlg_tests import TestRulesetDialogController

suite = ModuleTestRunner()

suite.addTestList("API", [TestAPI('test_api_encodes_proper_json'),
                          TestAPI('test_api_raises_error_on_empty_username'),
                          TestAPI('test_api_raises_error_on_empty_auth_token'),
                          ])

suite.addTestList("Stats", [TestStats('test_get_times_with_delta'),
                            TestStats('test_get_times_includes_end_time_for_perfect_range'),
                            TestStats('test_get_times_does_not_include_end_time_for_imperfect_range'),
                            TestStats('test_get_times_raises_error_for_invalid_range'),
                            TestStats('test_get_times_raises_error_for_zero_delta'),
                            TestStats('test_get_times_raises_error_for_negative_delta'),
                            TestStats('test_get_times_with_valid_negative_delta'),
                            TestStats('test_plot_converts_datetime_to_float'),
                            ])

suite.addTestList("Log", [TestLog('test_add_entry_adds_an_entry'),
                          TestLog('test_add_entry_adds_correct_entry'),
                          TestLog('test_add_entry_raises_error_when_args_missing'),
                          TestLog('test_add_entry_raises_error_on_unknown_type'),
                          TestLog('test_add_entry_does_not_require_comment'),
                          TestLog('test_delete_entry_deletes_an_entry'),
                          TestLog('test_delete_entry_deletes_correct_entry'),
                          TestLog('test_score_scores_entry_properly'),
                          TestLog('test_score_scores_rereads_properly'),
                          TestLog('test_get_score_totals_all_scores'),
                          TestLog('test_get_score_totals_scores_of_a_given_type'),
                          TestLog('test_get_score_raises_error_on_unknown_type'),
                          TestLog('test_get_value_totals_unadjusted_scores'),
                          TestLog('test_get_value_raises_error_on_unknown_type'),
                          TestLog('test_get_entries_for_range_returns_list_of_entries_in_range'),
                          TestLog('test_score_list_scores_list_of_entries_properly'),
                          TestLog('test_find_entry_by_tstmp'),
                          ])

suite.addTestList("Entry", [TestTadokuEntry('test_entry_to_unicode_under_default_locale'),
                            TestTadokuEntry('test_entry_to_unicode_under_ja_locale'),
                            ])

suite.addTestList("UI-MAIN",  
                         [TestMainWindowController('test_submit_clicked_submits_proper_entry'),
                          TestMainWindowController('test_submit_clicked_does_not_submit_on_value_error'),
                          TestMainWindowController('test_submit_clicked_does_not_clear_entries_on_error'),
                          TestMainWindowController('test_get_entry_type_returns_correct_type'),
                          TestMainWindowController('test_get_entry_type_accounts_for_dr_check'),
                          TestMainWindowController('test_parse_entry_value_parses_float_from_str'),
                          TestMainWindowController('test_parse_entry_value_parses_expression'),
                          TestMainWindowController('test_parse_entry_value_parses_time'),
                          TestMainWindowController('test_score_updates_on_entry'),
                          ])
suite.addTestList("UI-DIALOG",
                  [TestRulesetDialogController('test_accept_updates_ruleset_from_ui'),
                   TestRulesetDialogController('test_accept_does_not_update_with_invalid_input'),
                  ])

if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.CRITICAL)
    suite.run()