# -*- coding: utf-8 -*-

import unittest
import datetime

from tadokutan import stats

class TestStats(unittest.TestCase):

    def setUp(self):
        pass

    def test_get_times_with_delta(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(1)

        self.assertEquals(len(stats.get_times_with_delta(
            dt1, dt2, delta)), 6)

    def test_get_times_includes_end_time_for_perfect_range(self):
        # If the delta divides evenly over the whole range, we
        # should include the end time in the result
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(1)

        self.assertEquals(stats.get_times_with_delta(dt1, dt2, delta)[-1],
            dt2)

    def test_get_times_does_not_include_end_time_for_imperfect_range(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(2)

        self.assertNotEquals(stats.get_times_with_delta(dt1, dt2, delta)[-1],
            dt2)

    def test_get_times_raises_error_for_invalid_range(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 1)
        delta = datetime.timedelta(1)

        with self.assertRaises(ValueError):
            stats.get_times_with_delta(dt1, dt2, delta)

    def test_get_times_raises_error_for_zero_delta(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(0)

        with self.assertRaises(ValueError):
            stats.get_times_with_delta(dt1, dt2, delta)

    def test_get_times_raises_error_for_negative_delta(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(-1)

        with self.assertRaises(ValueError):
            stats.get_times_with_delta(dt1, dt2, delta)

    def test_get_times_with_valid_negative_delta(self):
        dt1 = datetime.datetime(2013, 3, 10)
        dt2 = datetime.datetime(2013, 3, 5)
        delta = datetime.timedelta(-1)

        self.assertEquals(stats.get_times_with_delta(dt1, dt2, delta)[0],
            dt1)

    def test_plot_converts_datetime_to_float(self):
        dt1 = datetime.datetime(2013, 3, 5)
        dt2 = datetime.datetime(2013, 3, 10)
        delta = datetime.timedelta(1)

        plot = [(dt, 0) for dt in stats.get_times_with_delta(dt1, dt2, delta)]
        plot = stats.plot_datetime_to_float(plot)
        self.assertEquals(plot[0][0], 0)
        self.assertEquals(plot[1][0], 60*60*24)
        self.assertEquals(plot[-1][0], 60*60*24*5)

    def tearDown(self):
        pass