# -*- coding: utf-8 -*-

import unittest
import datetime

from tadokutan import api
from tadokutan import log
from tadokutan import settings

class TestAPI(unittest.TestCase):

    def setUp(self):
        self.entry = log.TadokuEntry(100, 
                                    "book", 
                                    1, 
                                    datetime.datetime.now(), 
                                    "Test comment.")
        settings.API_SETTINGS['username']   = "TestUser"
        settings.API_SETTINGS['auth_token'] = "TESTAUTH"

    def test_api_raises_error_on_empty_username(self):
        settings.API_SETTINGS['username'] = " "
        with self.assertRaises(api.TadokuAPIInvalidSetting):
            api.create_api_json(self.entry)

    def test_api_raises_error_on_empty_auth_token(self):
        settings.API_SETTINGS['auth_token'] = " "
        with self.assertRaises(api.TadokuAPIInvalidSetting):
            api.create_api_json(self.entry)

    def test_api_encodes_proper_json(self):
        JSON_EXPECT = ('{"username": "TestUser", "auth_token": "TESTAUTH", ' +
                       '"update": {"type": "book", "value": 100, "times": 1}}')
        encoded = api.create_api_json(self.entry)
        self.assertEquals(JSON_EXPECT, encoded)

    def tearDown(self):
        del self.entry
