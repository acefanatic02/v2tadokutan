# -*- coding: utf-8 -*-

import unittest

from tadokutan.controllers.mainwindow import MainWindowController
from tadokutan import log
from tadokutan import settings

from tests.mocks.qtmocks import *

class TestMainWindowController(unittest.TestCase):

    def setUp(self):
        win_mock = WindowMock()

        win_mock.ui.score = LabelMock()
        
        win_mock.ui.book_radio      = RadioButtonMock()
        win_mock.ui.manga_radio     = RadioButtonMock()
        win_mock.ui.game_radio      = RadioButtonMock()
        win_mock.ui.flgame_radio    = RadioButtonMock()
        win_mock.ui.net_radio       = RadioButtonMock()
        win_mock.ui.news_radio      = RadioButtonMock()
        win_mock.ui.nico_radio      = RadioButtonMock()
        win_mock.ui.subs_radio      = RadioButtonMock()
        win_mock.ui.lyric_radio     = RadioButtonMock()
        win_mock.ui.sent_radio      = RadioButtonMock()
        win_mock.ui.book_dr_check   = CheckboxMock()

        win_mock.ui.media_bgroup = ButtonGroupMock(
               [win_mock.ui.book_radio,
                win_mock.ui.manga_radio,
                win_mock.ui.game_radio,
                win_mock.ui.flgame_radio,
                win_mock.ui.net_radio,
                win_mock.ui.news_radio,
                win_mock.ui.nico_radio,
                win_mock.ui.subs_radio,
                win_mock.ui.lyric_radio,
                win_mock.ui.sent_radio,]
            )

        ruleset = {'book':1, 'book_dr':1.48, 'manga':0.2, 'game':0.05, 'subs':0.2}
        test_log = log.ReadingLog(ruleset)
        
        settings.SETTINGS['last_opened_file'] = "" # Avoid key error
        self.mwc = MainWindowController(win_mock, log=test_log)

    def test_submit_clicked_submits_proper_entry(self):
        self.mwc.window.ui.amount_read = TextInputMock("100")
        self.mwc.window.ui.times_read = TextInputMock("1")
        self.mwc.window.ui.entry_comment = TextInputMock("Comment!")
        self.mwc.window.ui.book_radio.checked = True

        settings.SETTINGS['save_on_entry_submit'] = False

        self.mwc.submit_clicked()

        self.assertEquals(self.mwc.log.entries[-1].value, 100)
        self.assertEquals(self.mwc.log.entries[-1].type, 'book')
        self.assertEquals(self.mwc.log.entries[-1].times, 1)
        self.assertEquals(self.mwc.log.entries[-1].comment, "Comment!")

    def test_submit_clicked_does_not_submit_on_value_error(self):
        self.mwc.window.ui.amount_read = TextInputMock("NOTVALIDNUMBER")
        self.mwc.window.ui.times_read = TextInputMock("1")
        self.mwc.window.ui.entry_comment = TextInputMock("Comment!")
        self.mwc.window.ui.book_radio.checked = True

        self.mwc.submit_clicked()

        self.assertEquals(len(self.mwc.log.entries), 0)

    def test_submit_clicked_does_not_clear_entries_on_error(self):
        self.mwc.window.ui.amount_read = TextInputMock("NOTVALIDNUMBER")
        self.mwc.window.ui.times_read = TextInputMock("1")
        self.mwc.window.ui.entry_comment = TextInputMock("Comment!")
        self.mwc.window.ui.book_radio.checked = True

        self.mwc.submit_clicked()

        self.assertEquals(self.mwc.window.ui.amount_read.text(),
            "NOTVALIDNUMBER")
        self.assertEquals(self.mwc.window.ui.times_read.text(), "1")
        self.assertEquals(self.mwc.window.ui.entry_comment.text(),
            "Comment!")

    def test_get_entry_type_returns_correct_type(self):
        self.mwc.window.ui.book_radio.checked = True
        self.assertEquals(self.mwc.get_entry_type(), 'book')
        self.mwc.window.ui.book_radio.checked = False

        self.mwc.window.ui.manga_radio.checked = True
        self.assertEquals(self.mwc.get_entry_type(), 'manga')
        self.mwc.window.ui.manga_radio.checked = False

    def test_get_entry_type_accounts_for_dr_check(self):
        self.mwc.window.ui.book_radio.checked = True
        self.mwc.window.ui.book_dr_check.checked = True
        self.assertEquals(self.mwc.get_entry_type(), 'book_dr')
        self.mwc.window.ui.book_radio.checked = False
        self.mwc.window.ui.book_dr_check.checked = False

    def test_parse_entry_value_parses_float_from_str(self):
        self.assertEquals(self.mwc.parse_entry_value("170.5", 'book'), 170.5)

    def test_parse_entry_value_parses_expression(self):
        expr_add = "(100 + 50)"
        expr_sub = "(100 - 50)"
        expr_fadd = "(100.0 + 50.0)"
        expr_fsub = "(100.0 - 50.0)"

        for expr in [expr_add, expr_fadd]:
            self.assertEquals(self.mwc.parse_entry_value(expr, 'book'),
                150.0)

        for expr in [expr_sub, expr_fsub]:
            self.assertEquals(self.mwc.parse_entry_value(expr, 'book'),
                50.0)

    def test_parse_entry_value_parses_time(self):
        time_a = "05:00:30"
        time_a_expect = 300.5

        self.assertEquals(self.mwc.parse_entry_value(time_a, 'subs'), time_a_expect)

        time_b = "3:00:30"
        time_b_expect = 180.5

        self.assertEquals(self.mwc.parse_entry_value(time_b, 'subs'), time_b_expect)

    def test_score_updates_on_entry(self):
        self.mwc.window.ui.amount_read = TextInputMock("100")
        self.mwc.window.ui.times_read = TextInputMock("1")
        self.mwc.window.ui.entry_comment = TextInputMock("Comment!")
        self.mwc.window.ui.book_radio.checked = True

        settings.SETTINGS['save_on_entry_submit'] = False
        self.mwc.submit_clicked()

        self.assertEquals(self.mwc.window.ui.score.data, "100.0")

        self.mwc.window.ui.amount_read = TextInputMock("100")
        self.mwc.window.ui.times_read = TextInputMock("1")
        self.mwc.window.ui.entry_comment = TextInputMock("Comment!")
        self.mwc.window.ui.book_radio.checked = True
        self.mwc.window.ui.book_dr_check.checked = True

        self.mwc.submit_clicked()

        self.assertEquals(self.mwc.window.ui.score.data, "248.0")
        
    def tearDown(self):
        del self.mwc
