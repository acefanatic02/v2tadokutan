# -*- coding: utf-8 -*-

class UiMock(object):

    def __init__(self):
        pass

class WindowMock(object):

    def __init__(self):
        super(WindowMock, self).__init__()
        self.ui = UiMock()

class ButtonGroupMock(object):

    def __init__(self, buttons):
        super(ButtonGroupMock, self).__init__()
        self.buttons = buttons

    def checkedButton(self):
        for button in self.buttons:
            if button.isChecked():
                return button
        else:
            return 0

class RadioButtonMock(object):

    def __init__(self):
        super(RadioButtonMock, self).__init__()
        self.checked = False

    def isChecked(self):
        return self.checked

class CheckboxMock(object):

    def __init__(self):
        super(CheckboxMock, self).__init__()
        self.checked = False

    def isChecked(self):
        return self.checked

class TextInputMock(object):

    def __init__(self, data=None):
        super(TextInputMock, self).__init__()
        self.data = data

    def text(self):
        return self.data

    def clear(self):
        self.data = None

class LabelMock(object):

    def __init__(self, data=None):
        super(LabelMock, self).__init__()
        self.data = data

    def setText(self, text):
        self.data = text
