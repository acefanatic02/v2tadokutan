# -*- coding: utf-8 -*-

import unittest
import datetime
import time

from tadokutan import log

class TestLog(unittest.TestCase):

    def setUp(self):
        ruleset = {"book":1,"manga":0.2}
        self.log = log.ReadingLog(ruleset)

    def test_add_entry_adds_an_entry(self):
        self.assertEquals(len(self.log.entries), 0)
        self.log.add_entry(value=100,
                           type='book',
                           times=1,
                           comment="Test Comment")
        self.assertEquals(len(self.log.entries), 1)

    def test_add_entry_adds_correct_entry(self):
        self.log.add_entry(value=100,
                           type='book',
                           times=1,
                           comment="Test Comment")
        entry = self.log.entries[-1]

        self.assertEquals(entry.value, 100)
        self.assertEquals(entry.type, 'book')
        self.assertEquals(entry.times, 1)
        self.assertEquals(entry.comment, "Test Comment")

    def test_add_entry_raises_error_when_args_missing(self):
        with self.assertRaises(TypeError):
            self.log.add_entry(comment="Test Comment")

    def test_add_entry_raises_error_on_unknown_type(self):
        with self.assertRaises(TypeError):
            self.log.add_entry(value=100,
                               type='unknown',
                               times=1,
                               comment="Test Comment")

    def test_add_entry_does_not_require_comment(self):
        self.log.add_entry(value=100,
                           type='book',
                           times=1)
        entry = self.log.entries[-1]
        self.assertEquals(entry.comment, u"")

    def test_add_entry_dirties_log(self):
        self.assertFalse(self.log.dirty)
        self.log.add_entry(value=100, type='book', times=1)
        self.assertTrue(self.log.dirty)

    def test_delete_entry_deletes_an_entry(self):
        self.log.add_entry(value=100,
                           type='book',
                           times=1,
                           comment="Test Comment")
        self.assertEquals(len(self.log.entries), 1)

        entry_time = self.log.entries[-1].created_at
        self.log.delete_entry(entry_time)

        self.assertEquals(len(self.log.entries), 0)

    def test_delete_entry_deletes_correct_entry(self):
        self.log.add_entry(value=100,
                           type='book',
                           times=1,
                           comment="First Entry")
        time.sleep(0.01)
        self.log.add_entry(value=20,
                           type='book',
                           times=1,
                           comment="Second Entry")
        time.sleep(0.01)
        self.log.add_entry(value=30,
                           type='book',
                           times=1,
                           comment="Third Entry")

        self.assertEquals(len(self.log.entries), 3)

        delete_time = self.log.entries[1].created_at  # Second entry
        self.log.delete_entry(delete_time)

        self.assertEquals(self.log.entries[0].comment, "First Entry")
        self.assertEquals(self.log.entries[1].comment, "Third Entry")

    def test_delete_entry_dirties_log(self):
        self.log.add_entry(value=100, type='book', times=1)
        self.log.dirty = False  # add_entry dirties the log as well, we need to clear it

        delete_time = self.log.entries[0].created_at
        self.log.delete_entry(delete_time)

        self.assertTrue(self.log.dirty)

    def test_update_ruleset_replaces_ruleset(self):
        self.log.update_ruleset({'foo':10})
        self.assertEquals(self.log.ruleset['foo'], 10)
        with self.assertRaises(KeyError):
            self.log.ruleset['book']

    def test_update_ruleset_dirties_log(self):
        self.assertFalse(self.log.dirty)
        self.log.update_ruleset({'foo':10})
        self.assertTrue(self.log.dirty)

    def test_score_scores_entry_properly(self):
        entry_book  = log.TadokuEntry(100, 'book', 1, datetime.datetime.now(), "")
        entry_manga = log.TadokuEntry(100, 'manga', 1, datetime.datetime.now(), "")
        self.assertEquals(self.log._score(entry_book), 100)
        self.assertEquals(self.log._score(entry_manga), 20)

    def test_score_scores_rereads_properly(self):
        entry_second = log.TadokuEntry(100, 'book', 2, datetime.datetime.now(), "")
        entry_third  = log.TadokuEntry(100, 'book', 3, datetime.datetime.now(), "")
        entry_fourth = log.TadokuEntry(100, 'book', 4, datetime.datetime.now(), "")

        self.assertEquals(self.log._score(entry_second), 50.0)
        self.assertEquals(self.log._score(entry_third),  25.0)
        self.assertEquals(self.log._score(entry_fourth), 12.5)

    def test_get_score_totals_all_scores(self):
        self.log.add_entry(value=100, type='book', times=1)
        self.log.add_entry(value=100, type='manga', times=1)
        self.assertEquals(self.log.get_score(), 120)

    def test_get_score_totals_scores_of_a_given_type(self):
        self.log.add_entry(value=100, type='book', times=1)
        self.log.add_entry(value=100, type='manga', times=1)
        self.assertEquals(self.log.get_score(['manga']), 20)

    def test_get_score_raises_error_on_unknown_type(self):
        with self.assertRaises(TypeError):
            self.log.get_score(['unknown'])

    def test_get_value_totals_unadjusted_scores(self):
        self.log.add_entry(value=100, type='manga', times=1)
        self.assertEquals(self.log.get_value('manga'), 100)

    def test_get_value_raises_error_on_unknown_type(self):
        with self.assertRaises(TypeError):
            self.log.get_value(['unknown'])

    def test_get_entries_for_range_returns_list_of_entries_in_range(self):
        self.log.add_entry(value=100, type='book', times=1)
        time.sleep(0.01)
        start_time = datetime.datetime.now()
        time.sleep(0.01)
        self.log.add_entry(value=100, type='book', times=1)
        self.log.add_entry(value=100, type='book', times=1)
        time.sleep(0.01)
        stop_time = datetime.datetime.now()
        time.sleep(0.01)
        self.log.add_entry(value=100, type='book', times=1)

        self.assertEquals(len(self.log.get_entries_for_range(
            start_time, stop_time)), 2)

    def test_score_list_scores_list_of_entries_properly(self):
        entry_a = log.TadokuEntry(100, 'book', 1, datetime.datetime.now(), "")
        entry_b = log.TadokuEntry(100, 'manga', 1, datetime.datetime.now(), "")

        self.assertEquals(self.log.score_list([entry_a, entry_b]), 120)

    def test_find_entry_by_tstmp(self):
        entry_first  = log.TadokuEntry(100, 'book', 1, datetime.datetime.now(), "")
        time.sleep(0.01)
        entry_second = log.TadokuEntry(100, 'book', 2, datetime.datetime.now(), "")
        time.sleep(0.01)
        entry_third  = log.TadokuEntry(100, 'book', 3, datetime.datetime.now(), "")
        time.sleep(0.01)
        entry_fourth = log.TadokuEntry(100, 'book', 4, datetime.datetime.now(), "")
        for entry in (entry_first, entry_second, entry_third, entry_fourth):
            self.log.entries.append(entry)

        self.assertEquals(self.log._find_entry_by_tstmp(entry_first.created_at), 0)
        self.assertEquals(self.log._find_entry_by_tstmp(entry_second.created_at), 1)
        self.assertEquals(self.log._find_entry_by_tstmp(entry_third.created_at), 2)
        self.assertEquals(self.log._find_entry_by_tstmp(entry_fourth.created_at), 3)

    def tearDown(self):
        del self.log.entries
        del self.log
