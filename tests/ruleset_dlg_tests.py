# -*- coding: utf-8 -*-

import unittest

from tadokutan.controllers.ruleset_dlg import RulesetDialogController

from tests.mocks.qtmocks import *

class TestRulesetDialogController(unittest.TestCase):

    def setUp(self):
        win_mock = WindowMock()

        default_ruleset = {
            'book':      1,
            'book_dr':   1.48,
            'manga':     0.2,
            'game':      0.05,
            'fullgame':  0.16666666666666666,  # 1/6
            'net':       1,
            'news':      1,
            'nico':      0.1,
            'subs':      0.2,
            'lyric':     1,
            'sentences': 0.058823529411764705, # 1/17
        }

        win_mock.ui.book_val    = TextInputMock(default_ruleset['book'])
        win_mock.ui.book_dr_val = TextInputMock(default_ruleset['book_dr'])
        win_mock.ui.manga_val   = TextInputMock(default_ruleset['manga'])
        win_mock.ui.game_val    = TextInputMock(default_ruleset['game'])
        win_mock.ui.fgame_val   = TextInputMock(default_ruleset['fullgame'])
        win_mock.ui.net_val     = TextInputMock(default_ruleset['net'])
        win_mock.ui.news_val    = TextInputMock(default_ruleset['news'])
        win_mock.ui.nico_val    = TextInputMock(default_ruleset['nico'])
        win_mock.ui.subs_val    = TextInputMock(default_ruleset['subs'])
        win_mock.ui.lyric_val   = TextInputMock(default_ruleset['lyric'])
        win_mock.ui.sent_val    = TextInputMock(default_ruleset['sentences'])

        self.rdc = RulesetDialogController(win_mock, default_ruleset)

    def test_accept_updates_ruleset_from_ui(self):
        self.rdc.window.ui.fgame_val.data = "1"
        self.rdc.accept()

        self.assertEquals(self.rdc.ruleset['fullgame'], 1)

    def test_accept_does_not_update_with_invalid_input(self):
        self.rdc.window.ui.book_val.data = "foo"
        self.rdc.accept()

        self.assertEquals(self.rdc.ruleset['book'], 1)

    def tearDown(self):
        del self.rdc.window
        del self.rdc