# -*- coding: utf-8 -*-
import unittest
import datetime
import locale
import sys

from tadokutan.log import TadokuEntry

@unittest.skipIf(sys.platform.startswith("win32"), 
    "Tests break due to Windows' locale handling")
class TestTadokuEntry(unittest.TestCase):

    def setUp(self):
        self._locale = locale.getlocale()
        locale.setlocale(locale.LC_ALL, 'C')

    def test_entry_to_unicode_under_default_locale(self):
        dt = datetime.datetime(2013, 4, 22, 0, 0, 0)
        entry = TadokuEntry(100, 'book', 1, dt, "Test Comment!")
        expected = (u"Type:\tbook\nValue:\t100.00\nTimes:\t1\n"
                    u"Created:\tMon Apr 22 00:00:00 2013\nComment:\tTest Comment!\n")
        self.assertEquals(unicode(entry), expected)

    def test_entry_to_unicode_under_ja_locale(self):
        locale.setlocale(locale.LC_ALL, ('ja_JP', 'UTF-8'))
        dt = datetime.datetime(2013, 4, 22, 0, 0, 0)
        entry = TadokuEntry(100, 'book', 1, dt, "Test Comment!")
        expected = (u"Type:\tbook\nValue:\t100.00\nTimes:\t1\n"
                    u"Created:\t2013年04月22日 00時00分00秒\nComment:\tTest Comment!\n")
        self.assertEquals(unicode(entry), expected)

    def tearDown(self):
        locale.setlocale(locale.LC_ALL, self._locale)